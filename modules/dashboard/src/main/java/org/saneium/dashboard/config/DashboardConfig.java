package org.saneium.dashboard.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by mbollemeijer on 02/06/15.
 */
@Configuration
@EnableWebMvc
public class DashboardConfig {
}
